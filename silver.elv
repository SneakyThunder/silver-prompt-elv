use str;

var last-cmd-exit = 0
var last-cmd-duration = 0

fn add-after-command {|@hooks|
  each {|hook|
    if (not (has-value $edit:after-command $hook)) {
      set edit:after-command = [ $@edit:after-readline $hook ]
    }
  } $hooks
}

fn after-cmd-hook {|cmd|
  set last-cmd-duration = [(str:split . ""(* $cmd[duration] 1000))][0]
  set last-cmd-exit = (if (eq $cmd[error] $nil) { put 0 } else { put 1 })
}

fn init {|&lprompt= $true
          &rprompt= $true|
  if $lprompt {
    set edit:prompt = { env code=$last-cmd-exit cmdtime=$last-cmd-duration silver lprint }
  }
  if $rprompt {
    set edit:rprompt = { env code=$last-cmd-exit cmdtime=$last-cmd-duration silver rprint }
  }
  set-env VIRTUAL_ENV_DISABLE_PROMPT 1 # Doesn't make any sense yet
  add-after-command $after-cmd-hook~
}

if (not (has-external silver)) {
  put 'silver is not installed; please follow the instructions on https://github.com/reujab/silver#installation'
}
