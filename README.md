# silver plugin for elvish

> This is a fork of this (currently unmantained) [GitHub repo](https://github.com/silver-prompt/elvish)

## Dependencies
- [`elvish`](https://elv.sh/)
- [`silver`](https://github.com/reujab/silver/)

## Installation
1. Run
```
use epm
epm:install gitlab.com/SneakyThunder/silver-prompt-elv
```
2. In `~/.config/elvish/rc.elv` add
```
use gitlab.com/SneakyThunder/silver-prompt-elv/silver
silver:init &lprompt=$true &rprompt=$true
```
